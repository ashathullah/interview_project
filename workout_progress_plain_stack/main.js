var workouts = [
    { 
      no: 1,
      name: "workout1",
      completion_date: "jan 1 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: "" 
    },
    { 
      no: 2,
      name: "workout2",
      completion_date: "jan 2 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: "" 
    },
    { 
      no: 3,
      name: "workout3",
      completion_date: "jan 3 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: "" 
    },
    { 
      no: 4,
      name: "workout4",
      completion_date: "jan 4 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 1,
      badge_name: "intermediate" 
    },
    { 
      no: 5,
      name: "workout5",
      completion_date: "jan 5 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: "" 
    },
    { 
      no: 6,
      name: "workout6",
      completion_date: "jan 4 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 1,
      badge_name: "expert" 
    }
  ]

var dummyapi = [];

var count = 3;

var workout_container = document.getElementById("workouts");
//creating workout based on the number of objects present in
function createWorkouts(currentWorkout, index, parent_conteiner) {
    var workout = document.createElement("div");
    workout.className = "workout";
    workout.style.display = "inline-block";
    workout.style.position = "relative";
    workout.style.bottom = index*50 + "px";
    workout.style.backgroundColor = index < count? 'crimson' : 'violet' ;

//adding paragraph tag
    var workoutName = document.createElement("p");
    workoutName.innerHTML = currentWorkout.name;
    workout.appendChild(workoutName);

//adding badge
    if(currentWorkout.badge_flag === 1) {
        var badge = document.createElement('img');
        badge.src = "./Shield-Badge-Free-PNG-Image.png";
        badge.className = "badge";
        workout.appendChild(badge);
//adding badge_lable
        var badge_lable = document.createElement('span');
        badge_lable.innerHTML = currentWorkout.badge_name;
        badge_lable.className = "badgeLable";
        workout.appendChild(badge_lable);
    }
//adding current location
    if(currentWorkout.no === count) {
        var currentLevel = document.createElement('img');
        currentLevel.src = "./Location-Icon-PNG_ufs89t.png";
        currentLevel.className = "currentLevel";
        workout.appendChild(currentLevel)        
//adding lable
        var currentLocation_lable = document.createElement('span');
        currentLocation_lable.innerHTML = 'you are here';
        currentLocation_lable.className = "badgeLable";
        workout.appendChild(currentLocation_lable);
    }
    parent_conteiner.appendChild(workout);

}

function printD() {
    console.log(this.dummyapi);
}

function apiCall() {
    fetch('https://api.github.com/users')
    .then(res => {
        if(res.ok){
            console.log('success');
            return res.json()
        } else {
            console.log("failed")
        }
    })
    .then(data  => {
            this.dummyapi = data
            this.printD()
            return this.dummyapi;
            }
        )
    .catch(error => console.log(error))
};

(function(){
    this.apiCall()
    for(let i = 0; i < workouts.length ; i++) {
        createWorkouts(workouts[i],i, workout_container)
    }
})();
