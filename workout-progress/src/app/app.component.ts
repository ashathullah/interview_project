import { Component, OnInit } from '@angular/core';
import { ApiCallService } from './apicall.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public completedCount = 2;
  public workouts = [
    { 
      no: 1,
      name: "workout1",
      completion_date: "jan 1 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: ""
    },
    { 
      no: 2,
      name: "workout2",
      completion_date: "jan 2 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: "" 
    },
    { 
      no: 3,
      name: "workout3",
      completion_date: "jan 3 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: ""
    },
    { 
      no: 4,
      name: "workout4",
      completion_date: "jan 4 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 1,
      badge_name: "intermediate"
    },
    { 
      no: 5,
      name: "workout5",
      completion_date: "jan 5 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 0,
      badge_name: ""
    },
    { 
      no: 6,
      name: "workout6",
      completion_date: "jan 4 2022",
      calories_burnt: 100,
      duration: 30,
      badge_flag: 1,
      badge_name: "expert"
    }
  ]


  public dummyApi = [] ;

  constructor(private apiS: ApiCallService) {

  }

  ngOnInit() {

    this.apiS.getDummyData().subscribe(data => {
      this.dummyApi = data;
      console.log(data);
    })

    // for(let i = 0; i < this.workouts.length ; i++) {
    //   if(i < this.completedCount) {
    //     this.workouts[i].is_completed = true;
    //     console.log(this.workouts[i].is_completed)
    //   }
    //   else {
    //     this.workouts[i].is_completed = false;
    //     console.log(this.workouts[i].is_completed)
    //   }

    // }
  }
}
