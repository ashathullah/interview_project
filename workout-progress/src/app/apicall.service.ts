import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IApi } from './iapi'
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ApiCallService {
    private _url = "https://api.github.com/users";

    constructor(private http: HttpClient) {

    }

    getDummyData(): Observable<any> {
        return this.http.get<IApi[]>(this._url);
    }
}